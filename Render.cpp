#include "Includes.h"

//Copy pasted these drawing functions cos i'm too dumb to do that myself. Except for the drawEdgeRectangle function I coded on my own

struct SD3DVertex {
	float x, y, z, rhw;
	DWORD colour;
};

LPD3DXFONT mFont;

void CreateFont(LPDIRECT3DDEVICE9 dev)
{
	D3DXCreateFont(dev, 16, 0, FW_NORMAL, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial ", &mFont);
}


void cRender::Line(LPDIRECT3DDEVICE9 Dev, int x, int y, int x2, int y2, D3DCOLOR colour)
{
	SD3DVertex pVertex[2] = { { x, y, 0.0f, 1.0f, colour }, { x2, y2, 0.0f, 1.0f, colour } };

	Dev->SetPixelShader(NULL);
	Dev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);

	Dev->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
	Dev->DrawPrimitiveUP(D3DPT_LINELIST, 1, &pVertex, sizeof(SD3DVertex));
}

void cRender::FilledBox(LPDIRECT3DDEVICE9 Dev, int x, int y, int width, int height, D3DCOLOR colour)
{
	Dev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	Dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	Dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	Dev->SetRenderState(D3DRS_LIGHTING, false);
	Dev->SetPixelShader(NULL);

	SD3DVertex pVertex[4] = { { x, y + height, 0.0f, 1.0f, colour }, { x, y, 0.0f, 1.0f, colour }, { x + width, y + height, 0.0f, 1.0f, colour }, { x + width, y, 0.0f, 1.0f, colour } };

	Dev->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
	Dev->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, pVertex, sizeof(SD3DVertex));
}

void cRender::DrawShadowText(LPDIRECT3DDEVICE9 dev, const char* text, int x, int y, D3DCOLOR color)
{
	if (!mFont)
		CreateFont(dev);

	RECT rect;

	SetRect(&rect, x + 1, y + 1, x + 1, y + 1);
	mFont->DrawText(NULL, text, -1, &rect, DT_NOCLIP | DT_LEFT, D3DCOLOR_ARGB(255, 0, 0, 0));

	SetRect(&rect, x, y, x, y);
	mFont->DrawText(NULL, text, -1, &rect, DT_NOCLIP | DT_LEFT, color);
}

void cRender::BorderedBox(LPDIRECT3DDEVICE9 Dev, int x, int y, int w, int h, int t, D3DCOLOR color)
{
	FilledBox(Dev, x, y, w, t, color);
	FilledBox(Dev, x, y, t, h, color);
	FilledBox(Dev, x + w, y, t, h, color);
	FilledBox(Dev, x, y + h, w + t, t, color);
}

void cRender::Circle(LPDIRECT3DDEVICE9 Dev, int x, int y, int radius, int points, D3DCOLOR colour)
{
}

void cRender::FilledCircle(LPDIRECT3DDEVICE9 Dev, int x, int y, int radius, int points, D3DCOLOR colour)
{
}

void cRender::DrawEdgeRectangleBox(LPDIRECT3DDEVICE9 Dev, D3DXVECTOR2 EntPosition, D3DXVECTOR2 EntHead, int thickness, D3DCOLOR color)
{
	int height = EntPosition.y - EntHead.y;
	int width = height / 2;

	//Top Line
	Line(Dev, EntPosition.x - width / 3, EntPosition.y - height, EntPosition.x - width / 2, EntPosition.y - height, color);
	Line(Dev, EntPosition.x + width / 3, EntPosition.y - height, EntPosition.x + width / 2, EntPosition.y - height, color);

	//Bot Line
	Line(Dev, EntPosition.x - width / 3, EntPosition.y, EntPosition.x - width / 2, EntPosition.y, color);
	Line(Dev, EntPosition.x + width / 3, EntPosition.y, EntPosition.x + width / 2, EntPosition.y, color);


	//Left Line
	Line(Dev, EntPosition.x - width / 2, (EntPosition.y - height) + (width / 4), EntPosition.x - width / 2, EntPosition.y - height, color);
	Line(Dev, EntPosition.x - width / 2, (EntPosition.y - (width / 4)), EntPosition.x - width / 2, EntPosition.y, color);


	//right line
	Line(Dev, EntPosition.x + width / 2, (EntPosition.y - height) + (width / 4), EntPosition.x + width / 2, EntPosition.y - height, color);
	Line(Dev, EntPosition.x + width / 2, (EntPosition.y - (width / 4)), EntPosition.x + width / 2, EntPosition.y, color);
}

cRender Render;