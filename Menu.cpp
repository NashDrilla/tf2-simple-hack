#include "Includes.h"

int maxIndex;
int MenuX;
int MenuY = 530;

int MenuWidth = 150;
int MenuHeight = 250;

int CheckBoxWidth = 17;
int CheckBoxHeight = 17;

bool bShowMenu = true;

Buttons Button[20];

void Menu::AddButton(const char* bcaption, int bx, int by, int bw, int bh, bool status)
{
	Button[maxIndex].caption = bcaption;
	Button[maxIndex].x = bx;
	Button[maxIndex].y = by;
	Button[maxIndex].w = bw;
	Button[maxIndex].h = bh;
	Button[maxIndex].stat = status;

	maxIndex++;
}

void Menu::DrawButton(LPDIRECT3DDEVICE9 Dev)
{
	for (int i = 0; i < maxIndex + 1; i++)
	{
		int mw = Button[i].x + Button[i].w;
		int mh = Button[i].y + Button[i].h;

		//Defualt apperance
		Render.BorderedBox(Dev, Button[i].x, Button[i].y, Button[i].w, Button[i].h, 1, D3DCOLOR_ARGB(200, 220, 220, 220));
		Render.DrawShadowText(Dev, Button[i].caption, Button[i].x + 20, Button[i].y, D3DCOLOR_ARGB(255, 210, 210, 210));

		POINT cur;
		GetCursorPos(&cur);

		if (cur.x >= Button[i].x && cur.x <= mw && cur.y >= Button[i].y && cur.y <= mh)
		{
			Render.BorderedBox(Dev, Button[i].x, Button[i].y, Button[i].w, Button[i].h, 1, D3DCOLOR_ARGB(255, 255, 255, 255));
			Render.DrawShadowText(Dev, Button[i].caption, Button[i].x + 20, Button[i].y, D3DCOLOR_ARGB(255, 255, 255, 255));

			if (GetAsyncKeyState(VK_LBUTTON) & 1) //If I clicked mouse
			{
				Button[i].stat = !Button[i].stat;
			}
		}

		if (Button[i].stat)
		{
			Render.FilledBox(Dev, Button[i].x + 2, Button[i].y + 2, Button[i].w - 2, Button[i].h - 2, D3DCOLOR_ARGB(150, 0, 191, 255));
		}
	}
}

void Menu::InitMenu(LPDIRECT3DDEVICE9 Dev)
{

	static bool init = false;

	if (init == false)
	{
		//Team ESP Buttons
		AddButton("Enemy Box ESP", MenuX + 2, MenuY + 21, CheckBoxWidth, CheckBoxHeight, true); //0
		AddButton("Enemy Health Line", MenuX + 2, MenuY + 41, CheckBoxWidth, CheckBoxHeight, true); //1
		AddButton("Enemy Health Number", MenuX + 2, MenuY + 61, CheckBoxWidth, CheckBoxHeight, true); //2

		//Enemy ESP Buttons
		AddButton("Team Box ESP", MenuX + 2, MenuY + 111, CheckBoxWidth, CheckBoxHeight, false); //3
		AddButton("Team Enemy Health Line", MenuX + 2, MenuY + 131, CheckBoxWidth, CheckBoxHeight, false); //4
		AddButton("Team Health Number", MenuX + 2, MenuY + 151, CheckBoxWidth, CheckBoxHeight, false); //5

		//Triggerbot button
		AddButton("TriggerBot", MenuX + 2, MenuY + 201, CheckBoxWidth, CheckBoxHeight, true);//6

		//Crosshair
		AddButton("CrossHair", MenuX + 2, MenuY + 221, CheckBoxWidth, CheckBoxHeight, true);//7

		init = true;
	}


	if (GetAsyncKeyState(VK_INSERT) & 1)
	{
		bShowMenu = !bShowMenu;
	}

	if (bShowMenu)
	{
		//Menu Drawing
		Render.FilledBox(Dev, MenuX, MenuY, MenuWidth, MenuHeight, D3DCOLOR_ARGB(100, 0, 0, 0));
		Render.BorderedBox(Dev, MenuX - 1, MenuY - 1, MenuWidth + 1, MenuHeight + 1, 2, D3DCOLOR_ARGB(100, 0, 191, 255));
		Render.DrawShadowText(Dev ,"TF2 - Simple", MenuX + (MenuWidth / 2) - strlen("TF2 - Simple") * 7.0f / 2, MenuY - 15, D3DCOLOR_ARGB(255, 255, 255, 255));

		//Feature Drawing
		Render.DrawShadowText(Dev, "ESP", MenuX + (MenuWidth / 2) - strlen("Enemy ESP") * 7.0f / 2, MenuY + 1, D3DCOLOR_ARGB(220, 220, 220, 220));
		Render.DrawShadowText(Dev, "MISC", MenuX + (MenuWidth / 2) - strlen("MISC") * 7.0f / 2, MenuY + 181, D3DCOLOR_ARGB(220, 220, 220, 220));

		DrawButton(Dev);
	}
}
