#pragma once

namespace n_entityList // n_ = namespace cos im bad at naming things
{
	extern DWORD getEntity(int i);
	extern DWORD getEntityHealth(int i);
	extern DWORD getEntityTeam(int i);

	extern D3DXVECTOR3 getEntityLocation(int i);
	extern D3DXVECTOR3 getEntityHeadLocation(int i);
}