#include "Includes.h"

D3DXVECTOR2 cMath::World2Screen(D3DXVECTOR3 EnemyPosition, s_Matrix ViewMatrix)
{
	int Width = GetSystemMetrics(SM_CXSCREEN);
	int Height = GetSystemMetrics(SM_CYSCREEN);

	int X = Width - Width;
	int Y = Height - Height;

	D3DXVECTOR2 ScreenPosition;
	if (ViewMatrix.Matrix)
	{
		ScreenPosition.x = ViewMatrix.Matrix[0][0] * EnemyPosition.x + ViewMatrix.Matrix[0][1] * EnemyPosition.y + ViewMatrix.Matrix[0][2] * EnemyPosition.z + ViewMatrix.Matrix[0][3];
		ScreenPosition.y = ViewMatrix.Matrix[1][0] * EnemyPosition.x + ViewMatrix.Matrix[1][1] * EnemyPosition.y + ViewMatrix.Matrix[1][2] * EnemyPosition.z + ViewMatrix.Matrix[1][3];
		float w = ViewMatrix.Matrix[3][0] * EnemyPosition.x + ViewMatrix.Matrix[3][1] * EnemyPosition.y + ViewMatrix.Matrix[3][2] * EnemyPosition.z + ViewMatrix.Matrix[3][3];
		if (w > 0.01f)
		{
			float invw = 1.0f / w;
			ScreenPosition.x *= invw;
			ScreenPosition.y *= invw;
			float x = Width / 2.f;
			float y = Height / 2.f;
			x += 0.5f * ScreenPosition.x * Width + 0.5f;
			y -= 0.5f * ScreenPosition.y * Height + 0.5f;
			ScreenPosition.x = x + X;
			ScreenPosition.y = y + Y;
		}
		else
		{
			ScreenPosition.x = -1.f;
			ScreenPosition.y = -1.f;
		}
	}
	return ScreenPosition;
}