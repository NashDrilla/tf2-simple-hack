#include "Includes.h"

using namespace n_entityList;
using namespace n_LocalPlayer;

void cESP::InitESP(LPDIRECT3DDEVICE9 pDevice)
{
	for (int i = 0; i < 32; i++)
	{
		if (getEntity(i) != NULL && getEntityHealth(i) > 0 && getEntity(i) != getLocalPlayer())
		{
			D3DXVECTOR2 EntBase = World2Screen(getEntityLocation(i), getViewMatrix());
			D3DXVECTOR2 EntHead = World2Screen(getEntityHeadLocation(i), getViewMatrix());

			int health = getEntityHealth(i);

			int height = EntBase.y - EntHead.y;
			int width = height / 2;

			float healthPcnt = health / health;
			int HealthHeight = width * healthPcnt;

			if (getEntityTeam(i) != getLPTeam())
			{
				Render.DrawEdgeRectangleBox(pDevice, EntBase, EntHead, 1, D3DCOLOR_ARGB(255, 255, 0, 0));

				char HealthChar[255];
				sprintf_s(HealthChar, sizeof(HealthChar), "HP: %i", (int)health);
				Render.DrawShadowText(pDevice, HealthChar, EntBase.x - 20, EntBase.y + 4, D3DCOLOR_ARGB(255, 255, 0, 0));

				Render.FilledBox(pDevice, EntBase.x - width / 2, EntBase.y - 2, HealthHeight, 2, D3DCOLOR_ARGB(255, 255, 0, 0));
			}
		}
	}
}

cESP ESP;