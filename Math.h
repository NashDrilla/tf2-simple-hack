#pragma once

namespace cMath
{
	extern struct s_Matrix
	{
		float Matrix[4][4];
	};


	extern D3DXVECTOR2 World2Screen(D3DXVECTOR3 EnemyPosition, s_Matrix ViewMatrix);
}