#pragma once
class cRender
{
public:
	void Line(LPDIRECT3DDEVICE9 Dev, int x, int y, int x2, int y2, D3DCOLOR colour);
	void FilledBox(LPDIRECT3DDEVICE9 Dev, int x, int y, int width, int height, D3DCOLOR colour);

	void DrawShadowText(LPDIRECT3DDEVICE9 dev, const char* text, int x, int y, D3DCOLOR color);

	void BorderedBox(LPDIRECT3DDEVICE9 Dev, int x, int y, int w, int h, int t, D3DCOLOR colour);
	void Circle(LPDIRECT3DDEVICE9 Dev, int x, int y, int radius, int points, D3DCOLOR colour);
	void FilledCircle(LPDIRECT3DDEVICE9 Dev, int x, int y, int radius, int points, D3DCOLOR colour);
	void DrawEdgeRectangleBox(LPDIRECT3DDEVICE9 Dev, D3DXVECTOR2 EntPosition, D3DXVECTOR2 EntHead, int thickness, D3DCOLOR color);
};
extern cRender Render;