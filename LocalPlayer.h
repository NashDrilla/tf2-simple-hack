#pragma once
#include "Includes.h"

using namespace cMath;

namespace n_LocalPlayer
{
	extern DWORD getLocalPlayer();
	extern DWORD getLPhealth();
	extern DWORD getLPTeam();
	
	extern D3DXVECTOR3 getLPviewAngles();
	extern s_Matrix getViewMatrix();
}