#include "Includes.h"
using namespace Menu;

/*
To Do:

-Fix and make menu better
-Fix renderer / drawing so that I can blend colours
-Fix alignment in ESP
-Cleanup some code. Name things better etc.

-Add more ESP features, team check and health etc
-Implement a godly aimbot (If my brain allows me)
-Add bunnyhop (cos why not)
-Add Triggerbot if I can be bothered
-Find other shit to add

-Move project for CS:GO use.

-Have a bit of fun with it
*/




HRESULT(APIENTRY* oEndScene)(LPDIRECT3DDEVICE9 pDevice);

HRESULT APIENTRY hook_EndScene(LPDIRECT3DDEVICE9 pDevice)
{
	ESP.InitESP(pDevice);

	return oEndScene(pDevice);
}

DWORD WINAPI ThreadMain(LPVOID param)
{
	//AllocConsole();
	//freopen("CONOUT$", "w", stdout);


	DWORD dwAddress = procMem.GetSignature((char*)"d3d9.dll", (char*)"6A 14 B8 ? ? ? ? E8 ? ? ? ? 8B 7D 08 8B DF 8D 47 04 F7 DB 1B DB 23 D8 89 5D E0");

	oEndScene = (HRESULT(APIENTRY*)(LPDIRECT3DDEVICE9 pDevice)) dwAddress;

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)oEndScene, hook_EndScene);
	DetourTransactionCommit(); //Write the hook

	while (true)
	{
		if (GetAsyncKeyState(VK_END))
		{
			break;
		}
		Sleep(1);
	}


	//Unhook
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)oEndScene, hook_EndScene);
	DetourTransactionCommit(); //Write the hook

	Sleep(1000);

	//Uninject
	FreeLibraryAndExitThread((HMODULE)param, 0);

	
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hMod, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)&ThreadMain, hMod, 0, 0);
	}

	return TRUE;
}