#pragma once

//Shit used for pattern scanning
#define INRANGE(x,a,b)    (x >= a && x <= b)
#define getBits( x )    (INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
#define getByte( x )    (getBits(x[0]) << 4 | getBits(x[1]))
#define ABS(x) ((x<0) ? (-x) : (x))

class cProcMem
{
public:
    DWORD dwFindPattern(DWORD dwAddress, DWORD dwLength, const char* szPattern);

    DWORD GetSignature(const char* szModule, char* chPattern);

    DWORD GetModule(const char* moduleName);

    template <typename var>
    var WriteMemory(DWORD Address, var Value);

    // Basic RPM wrapper, easier to use.
    template <typename var>
    var ReadMemory(DWORD Address);

}; extern cProcMem procMem;