#include "Includes.h"

DWORD cProcMem::dwFindPattern(DWORD dwAddress, DWORD dwLength, const char* szPattern)
{
	{
		const char* pat = szPattern;
		DWORD firstMatch = NULL;
		for (DWORD pCur = dwAddress; pCur < dwLength; pCur++)
		{
			if (!*pat) return firstMatch;
			if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat)) {
				if (!firstMatch) firstMatch = pCur;
				if (!pat[2]) return firstMatch;
				if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?') pat += 3;
				else pat += 2;
			}
			else {
				pat = szPattern;
				firstMatch = 0;
			}
		}
		return NULL;
	}
}

DWORD cProcMem::GetSignature(const char* szModule, char* chPattern)
{
	HMODULE hmModule = GetModuleHandle(szModule);
	PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hmModule;
	PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((DWORD)hmModule) + pDOSHeader->e_lfanew);
	return dwFindPattern(((DWORD)hmModule) + pNTHeaders->OptionalHeader.BaseOfCode, ((DWORD)hmModule) + pNTHeaders->OptionalHeader.SizeOfCode, chPattern);
}

DWORD cProcMem::GetModule(const char* moduleName)
{
	HANDLE hModule = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
	MODULEENTRY32 mEntry;
	mEntry.dwSize = sizeof(mEntry);


	do
		if (!strcmp(mEntry.szModule, moduleName))
		{
			CloseHandle(hModule);
			return (DWORD)mEntry.modBaseAddr;
			std::cout << "Found Modules" << std::endl << std::endl;
		}
	while (Module32Next(hModule, &mEntry));

	std::cout << "\nCould not attach to Client.dll..\n\n";

	return 0;

}

template<typename var>
inline var cProcMem::WriteMemory(DWORD Address, var Value)
{
	return var();
}

template<typename var>
inline var cProcMem::ReadMemory(DWORD Address)
{
	return var();
}

cProcMem procMem;