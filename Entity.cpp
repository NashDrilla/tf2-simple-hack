#include "Includes.h"

using namespace n_gameModules;

DWORD n_entityList::getEntity(int i)
{
	return *(DWORD*)(clientModule + 0xC4B0D4 + i * 0x10);
}

DWORD n_entityList::getEntityHealth(int i)
{
	return *(DWORD*)(getEntity(i) + 0xA8);
}

DWORD n_entityList::getEntityTeam(int i)
{
	return *(DWORD*)(getEntity(i) + 0xB0);
}

D3DXVECTOR3 n_entityList::getEntityLocation(int i)
{
	return *(D3DXVECTOR3*)(getEntity(i) + 0x364);
}

D3DXVECTOR3 n_entityList::getEntityHeadLocation(int i)
{
	D3DXVECTOR3 EntHead;

	EntHead.x = getEntityLocation(i).x;
	EntHead.y = getEntityLocation(i).y;
	EntHead.z = getEntityLocation(i).z + 75;

	return EntHead;
}
