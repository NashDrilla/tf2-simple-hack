#include "LocalPlayer.h"

using namespace n_gameModules;

DWORD n_LocalPlayer::getLocalPlayer()
{
	return *(DWORD*)(clientModule + 0xC3D5B0);
}

D3DXVECTOR3 n_LocalPlayer::getLPviewAngles()
{
	return *(D3DXVECTOR3*)(clientModule + 0x364);
}

DWORD n_LocalPlayer::getLPhealth()
{
	return *(DWORD*)(getLocalPlayer() + 0xA8);
}

DWORD n_LocalPlayer::getLPTeam()
{
	return *(DWORD*)(getLocalPlayer() + 0xB0);
}

s_Matrix n_LocalPlayer::getViewMatrix()
{
	s_Matrix Matrix;

	memcpy(&Matrix, (PBYTE*)(engineModule + 0x59EB80), sizeof(Matrix));

	return Matrix;
}