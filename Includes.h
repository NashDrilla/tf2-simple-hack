#pragma once

//DirectX Includes
#include <d3d9.h>
#pragma comment (lib, "d3d9.lib")

#include <d3dx9.h>
#pragma comment (lib, "d3dx9.lib")

#include <detours.h>
#pragma comment(lib, "detours.lib")

//Main Includes
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <string>
#include <vector>
#include <sstream>
#include <cassert>

//Including my header files
#include "ProcMem.h"
#include "Modules.h"
#include "Math.h"
#include "Render.h"
#include "Menu.h"
#include "LocalPlayer.h"
#include "Entity.h"
#include "ESP.h"